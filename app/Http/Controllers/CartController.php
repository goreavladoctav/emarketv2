<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Inertia::render('Front/Cart/Index', [
            'products' =>  $request->session()->get('cart', []),
            'nrProducts' => count($request->session()->get('cart', []))
        ]);
    }

    public function add(Request $request, Product $product)
    {
        $cart = $request->session()->get('cart', []);
        $cart[$product->id] = [
            'product' => $product,
            'q' => 1
        ];
        $request->session()->put('cart', $cart);

        return redirect('/cosul-meu');
    }

    public function updateQuantity(Request $request, Product $product, $q)
    {
        $cart = $request->session()->get('cart', []);
        $cart[$product->id]['q'] = $q;
        $request->session()->put('cart', $cart);

        return redirect('/cosul-meu');
    }

    public function deleteProduct(Request $request, Product $product)
    {
        $cart = $request->session()->get('cart', []);
        unset($cart[$product->id]);
        $request->session()->put('cart', $cart);

        return redirect('/cosul-meu');

    }


}
