<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\PromotionController as AdminPromotionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProductController::class, 'index'])->name('acasa');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::name('admin.')->middleware('checkadmin')->prefix('admin')->group(function () {
    Route::get('/orders/filters', [AdminOrderController::class, 'filter']);
    Route::resource('products', AdminProductController::class );// am definit rutele pentru admin.
    Route::resource('categories', AdminCategoryController::class);
    Route::resource('orders', AdminOrderController::class);
    Route::resource('promotions', AdminPromotionController::class);

});

Route::resource('products', ProductController::class );
Route::get('/cosul-meu', [CartController::class, 'index']);
Route::get('/cosul-meu/adauga/{product}', [CartController::class, 'add']);
Route::resource('orders', OrderController::class);
Route::post('/update-quantity/{product}/{q}', [CartController::class, 'updateQuantity']);
Route::delete('/delete-product/{product}', [CartController::class, 'deleteProduct']);


